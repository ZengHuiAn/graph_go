package StructTable

import (
	"errors"
	"math"
	"sync"
)

var MAX_COUNT = 16

var OutVerticeError = errors.New("exceeding the maximum limit")
var HasEdgeError = errors.New("there is already a side here")

type Vector struct {
	X int
	Y int
}

type Graph struct {
	n, m  int //节点数和边数
	verts []*Vertice
	edges [][]int
	sync.Mutex
}

func NewGraph(max int, verts []string) *Graph {

	g := &Graph{}

	g.n = max
	g.m = 0

	for index := 0; index < max; index++ {
		g.edges = append(g.edges, make([]int, g.n))
	}

	for i := 0; i < len(verts); i++ {
		g.verts = append(g.verts, &Vertice{data: verts[i], visited: false})
	}
	return g
}
func (g *Graph) GetXMax() int {
	//
	var rows = int(math.Sqrt(float64(g.n)))
	return rows
}

func (g *Graph) AddEdge(x int, y int) error {

	if !g.IsValid(x, y) {
		return OutVerticeError
	}

	if g.HasEdge(x, y) {
		return HasEdgeError
	}
	g.edges[x][y] = 1
	g.edges[y][x] = 1
	g.m++
	return nil
}
func (g *Graph) HasEdge(x int, y int) bool {
	if !g.IsValid(x, y) {
		return false
	}
	return g.edges[x][y] == 1
}

func (g *Graph) GetEdges() [][]int {
	return g.edges
}

func (g *Graph) IsValid(v int, w int) bool {
	if v < 0 || v > g.n || w < 0 || w > g.n {
		return false
	}
	return true
}

func (g *Graph) GetAll(x int, y int) []*Vector {
	g.Lock()
	g.ResetAllVisited()
	var value = g.GetValue(x, y)
	var temp = Vector{x, y}
	var correctArrays = g.get(temp, value.data)
	g.Unlock()
	return correctArrays
}

func (g *Graph) ResetAllVisited() {
	for i := 0; i < len(g.verts); i++ {
		g.verts[i].visited = false
	}
}

func (g *Graph) get(pos Vector, data string) []*Vector {

	if !g.IsValid(pos.X, pos.Y) {
		return nil
	}

	var result []*Vector

	var vt = g.GetValue(pos.X, pos.Y)

	if vt == nil || vt.visited {
		return nil
	}

	if vt.data == data {
		result = append(result, &Vector{pos.X, pos.Y})
		vt.visited = true
	} else {
		return nil
	}

	var currentPoint = pos.Y*g.GetXMax() + pos.X
	adjacentPoints := g.GetAllPoint(currentPoint)
	for _, p := range adjacentPoints {
		var localPoint = p
		var x = localPoint - localPoint/g.GetXMax()*g.GetXMax()
		var y = (localPoint - x) / g.GetXMax()

		var vTemp = g.get(Vector{x, y}, data)
		if vTemp != nil {
			result = append(result, vTemp...)
		}
	}
	return result
}

func (g *Graph) GetAllPoint(point int) []int {
	//
	var adjacentPoints []int = nil
	for k, v := range g.edges[point] {

		if v == 1 {
			adjacentPoints = append(adjacentPoints, k)
		}
	}
	return adjacentPoints
}

func (g *Graph) GetValue(x int, y int) *Vertice {
	if !g.IsValid(x, y) {
		return nil
	}
	var index = int(math.Sqrt(float64(g.n)))*y + x
	if index < len(g.verts) {
		return g.verts[index]
	}
	return nil
}
