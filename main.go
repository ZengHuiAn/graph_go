package main

import (
	"fmt"
	"graph/src/StructTable"
)

var datas = []string{"B", "B", "R", "R", "R", "G", "G", "G", "R", "R", "R", "R", "G", "B", "R", "R"}

func main() {
	var counter = 16
	g := StructTable.NewGraph(counter, datas)

	var gMax = g.GetXMax()
	for index := 0; index < counter; index++ {
		//
		var max = (index) / 4

		var maxNum = (max + 1) * 4
		//将当前点连接到对应的点
		if index+1 < maxNum {
			_ = g.AddEdge(index, index+1)
		}

		if index+gMax < counter {
			////将当前点连接上面的点
			_ = g.AddEdge(index, index+gMax)
			//将当前点连接左上的点
			var newIndex = index + gMax
			if newIndex-1 >= maxNum {
				_ = g.AddEdge(index, newIndex-1)
			}
			//将当前点连接右上的点
			if newIndex+1 < maxNum+gMax {
				_ = g.AddEdge(index, newIndex+1)
			}
		}
	}

	var array = g.GetAll(2, 2)
	var msg = ""
	for i := 0; i < len(array); i++ {
		msg += fmt.Sprintf("%v", *array[i])
	}

	fmt.Println(msg)
}
